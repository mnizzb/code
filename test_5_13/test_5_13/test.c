#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
//模仿strlen计算字符串的长度
//int my_strlen(const char* arr)
//{
//	int count = 0;
//	while (*arr != '\0')
//	{
//		count++;
//		arr++;
//	}
//	return count;
//}
//int main()
//{
//	
//	//char arr[] = "abcdef";
//	char* arr = "abcdef";
//	int a = my_strlen(arr);
//	printf("%d\n", a);
//	return 0;
//}



//模仿strcat来拼接到目标空间
//目标空间要有足够大的空间来存放
//char* my_strcat(char* arr,  char* arr1)
//{
// char* ret=arr;
//	while (*arr != '\0')
//	{
//		arr++;
//	}
//	while(*arr1)
//	{
//		*arr = *arr1;
//		arr1++;
//		arr++;
//		
//	}
// return ret;
//}
//int main()
//{
//	char arr[20] = "abc";
//	char arr1[] = "def";
//	my_strcat(arr, arr1);
//	printf("%s\n", arr);
//	return 0;
//}


//模仿strcpy来复制到目标空间
//目标空间要有足够大的空间来存放

//char* my_strcpy(char* arr, const char* arr1)
//{
//	char* ret = arr;
//	while(*arr1)
//	{
//		*arr = *arr1;
//		arr++;
//		arr1++;
//	}
// *arr=*arr1;
//	return ret;
//
//}
//int main()
//{
//	char arr[20] = "hello world";
//	char arr1[] = "def";
//	my_strcpy(arr, arr1);
//	printf("%s\n", my_strcpy);
//	return 0;
//}


//strcmp
#include <assert.h>
int my_strcmp(char* arr, char* arr1)
{
	assert(arr && arr1);
	while (*arr == *arr1)
	{
		if (*arr == '\0')
			return 0;
		arr++;
		arr1++;
	}
	return (*arr - *arr1);

	
}
int main()
{

	char* arr = "abcd";
	char* arr1 = "abd";
	int a=my_strcmp(arr, arr1);
	printf("%d\n", a);
	return 0;
}