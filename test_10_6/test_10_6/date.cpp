#define _CRT_SECURE_NO_WARNINGS
#include"date.h"

	Date::Date(int year, int month , int day )
	{
		_year = year;
		_month = month;
		_day = day;
	}
	Date::Date(const Date& d)
	{
		cout << "Date(Date& d)" << endl;
		_year = d._year;
		_month = d._month;
		_day = d._day;
	}
	void Date::Print()
	{
		cout << _year << "/" << _month << "/" << _day << endl;
	}

	//~Date()
	//{
	//	_year = _month = _day = 0;
	//	cout << "~Date()" << endl;
	//}
	bool Date::operator<(const Date& d)
	{
		if (_year < d._year)
		{
			return true;
		}
		else if (_year == d._year && _month < d._month)
		{
			return true;
		}
		else if (_year == d._year && _month == d._month && _day < d._day)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	bool Date::operator==(const Date& d)
	{
		return _year == d._year && _month == d._month && _day == d._day;
	}
	bool Date::operator<=(const Date& d)
	{
		return *this < d || *this == d;
	}
	bool Date::operator>(const Date& d)
	{
		return !(*this <= d);
	}
	bool Date::operator>=(const Date& d)
	{
		return !(*this < d);
	}
	bool Date::operator!=(const Date& d)
	{
		return !(*this == d);
	}
	int Date::GetMonthDay(int year, int month)
	{
		int montharray[] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };

		if (month == 2 && ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)))
		{
			return 29;
		}
		return montharray[month];
	}
	Date& Date::operator+=(int day)
	{
		if (_day < 0)
		{
			return *this -= (-day);
		}
		_day += day;
		while (_day > GetMonthDay(_year, _month))
		{
			// 月进位

			_day -= GetMonthDay(_year, _month);
			++_month;
			//月满了
			if (_month == 13)
			{
				++_year;
				_month = 1;
			}
		}
		return *this;
	}
	/*Date Date::operator+(int day)
	{
		Date tmp(*this);
		tmp += day;
		return tmp;
	}*/
	Date Date::operator+(int day)
	{
		Date tmp(*this);
		//Date tmp=*this;
		tmp += day;
		while (tmp > GetMonthDay(tmp._year, tmp._month))
		{
			tmp -= GetMonthDay(tmp._year, tmp._month);
			++_month;
			if (_month == 13)
			{
				++_year;
				_month = 1;
			}
		}
		return tmp;
	}
	Date& Date::operator-=(int day)
	{
		if (day < 0)
		{
			return *this += (-day);
		}
		_day -= day;
		while (_day <= 0)
		{
			--_month;
			if (_month == 0)
			{
				--_year;
				_month=12;
			}
			_day += GetMonthDay(_year, _month);
		}
		return *this;
	}
	Date Date::operator-(int day)
	{
		Date tmp(*this);
		tmp -= day;
		return tmp;
	}
	Date& Date::operator++()
	{
		*this += 1;
		return *this;
	}
	Date Date::operator++(int)
	{
		Date tmp(*this);
		*this += 1;
		return tmp;
	}
	Date& Date::operator--()
	{
		*this -= 1;
		return *this;
	}
	Date Date::operator--(int)
	{
		Date tmp(*this);
		*this -= 1;
		return tmp;
	}
	//d1-d2;
	int Date::operator-(const Date& d)
	{
		Date max = *this;
		Date min = d;
		int flag = 1;
		if (*this < d)
		{
			max = d;
			min = *this;
			flag = -1;
		}
		int n = 0;
		while (min != max)
		{
			++min;
			++n;
		}
		return n * flag;
	}
