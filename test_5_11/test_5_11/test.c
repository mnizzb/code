#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
//第一行输入一个正整数 n ，表示数组的长度
//第二行输入 n 个正整数，表示数组中每个数字的值
int cal(int* array, int n)
{
	int i = 0;
	int tmp = 0;
	for (i = 0; i < n; i++)
	{
		scanf("%d", &array[i]);
	}
	for (i = 0; i < n; i++)
	{
		tmp += *(array + i);
	}
	return tmp;
}
int main()
{
	int array[1000];
	int n = 0;
	scanf("%d", &n);

	int a=cal(array, n);
	printf("%d ", a);
	return 0;
}