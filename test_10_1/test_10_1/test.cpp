#include<iostream>
using namespace std;
typedef int DataType;
struct Stack
{
	void Init(size_t capacity)
	{
		_array = (DataType*)malloc(sizeof(DataType) * capacity);
		if (_array == nullptr)
		{
			perror("malloc fail");
			return;
		}
		_capacity = capacity;
		_size = 0;
	} 
	void push(const DataType& data)
	{
		_array[_size] = data;
		++_size;
	}
	DataType Top()
	{
		return _array[_size - 1];
	}
	void Destroy()
	{
		if (_array)
		{
			free(_array);
			_array = nullptr;
			_capacity = 0;
			_size = 0;
		}
	}
	DataType* _array;
	size_t _capacity;
	size_t _size;
};

//class Person
//{
//public:
//	void showInfo()
//	{
//		cout << _name << "-" << _sex << "-" << _age << endl;
//	}
//
//	char* _name;
//	char* _sex;
//	int _age;
//};
class Person
{
public:
	void PrintPersinInfo();
private:
	char _name[20];
	char _gender[3];
	int _age;
};
//这里需要指定PrintPersinInfo属于Person这个类域
void Person::PrintPersinInfo()
{
	cout << _name << " " << _gender << " " << _age << endl;
}
//int main()
//{
//	Stack s;;
//	s.Init(10);
//	s.push(1);
//	s.push(2);
//	s.push(3);
//	s.push(4);
//	cout << s.Top() << endl;
//	s.Destroy();
//
//
//	return 0;
//}
class A
{
public:
	void PrintA()
	{
		cout << _a << endl;
	}
private:
	char _a;
};

class Date
{
public:
	void Init(int year, int month, int day)
	{
		_year = year;
		_month = month;
		_day = day;
	}
	void Print()
	{
		cout << _year << "-" << _month << "-" << _day << endl;
	}

private:
	int _year;
	int _month;
	int _day;
};
//int main()
//{
//	Date d1, d2;
//	d1.Init(2024, 10, 2);
//	d2.Init(2024, 10, 3);
//	d1.Print();
//	d2.Print();
//	return 0;
//}
int& ADD(int a, int b)
{
	int c = a + b;
	return c;
}

int main()
{
	int ret = ADD(1, 2);
	ADD(3, 4);
	cout << ret << endl;
}
