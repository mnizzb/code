#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>

typedef int STData;
typedef  struct STnode
{
	STData data;
	struct STnode* next;
}STnode;

void Print(STnode* phead);//打印链表

void PushFront(STnode** pphead, STData x);//头插
void PushBack(STnode** pphead, STData x);//尾插

void PopFront(STnode** pphead);
void PopBack(STnode** pphead);

STnode* Find(STnode* phead, STData x);

void Insert(STnode** pphead, STnode* pos, STData x);//在pos之前插入

void InsertAfter( STnode* pos, STData x);//在pos之后插入

//删除当前pos的值
void Erase(STnode** pphead,STnode* pos);
//删除pos后的值
void EraseAfter(STnode* pos);
