#define _CRT_SECURE_NO_WARNINGS
//求出0～100000之间的所有“水仙花数”并输出。
//#include <stdio.h>
//int main()
//{
//	int num = 0;
//	int x = 0;
//	int y = 0;
//	int z = 0;
//	int arr = 0;
//	
//	scanf("%d", &num);
//	if (num < 100 || num > 999) {
//		printf("不是水仙花数\n");
//		return 0;
//	}
//	int num2 = num;
//		x = num % 10;
//		num = num / 10;
//		y = num % 10;
//		num = num / 10;
//		z = num % 10;
//		num = num / 10;
//	
//	arr =(x*x*x)  + (y*y*y)+ (z*z*z);
//	if (num2 == arr)
//		printf("是水仙花数");
//	else
//		printf("不是水仙花数");
//	return 0;
//}
//优化版本
#include <stdio.h>
#include <math.h>
int main()
{
	int i = 0;

	for (i = 0; i <= 100000; i++)
	{
		int n = 1;
		int tmp = i;
		while (tmp /= 10)
		{
			n++;
		}
		tmp = i;
		int sum = 0;
		while(tmp)
		{
			sum += pow(tmp % 10 , n );
			tmp /= 10;
		}
		if (sum == i)
			printf("%d ", i);

	}
		
	
	return 0;
}

//求前5项的值，例：sn=2+22+222+2222+22222
//#include <stdio.h>
//int main()
//{
//	int i = 0;
//	int num = 0;
//	scanf("%d", &num);
//	int sn = num;
//	for (i = 1; i < 5; i++)
//	{
//		int tmp = 2;
//		num = num * 10 + tmp;
//		sn += num ;
//	
//	
//	}
//	printf("%d\n", sn);
//	return 0;
//}

//优化版本
//#include<stdio.h>
//int main()
//{
//	int i = 0;
//	int num = 0;
//	int a = 0;
//	int n = 0;
//	int k = 0;
//	scanf("%d %d", &a, &n);
//	for (i = 0; i < n; i++)
//	{
//		k = k * 10 + a;//针对水仙花数比较简单的算法
//		num += k;
//
//
//	}
//	printf("%d\n", num);
//	return 0;
//}