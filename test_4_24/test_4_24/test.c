#define _CRT_SECURE_NO_WARNINGS
//写一个函数打印arr数组的内容，不使用数组下标，使用指针。arr是一个整形一维数组。
//#include <stdio.h>
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,0 };
//	int* ptr = arr;
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", *(ptr + i));
//	}
//
//	return 0;
//
//}

//
//#include <stdio.h>
//int main()
//{
//    int arr[] = { 1,2,3,4,5 };
//    short* p = (short*)arr;
//    int i = 0;
//    for (i = 0; i < 4; i++)
//    {
//        *(p + i) = 0;
//    }
//
//    for (i = 0; i < 5; i++)
//    {
//        printf("%d ", arr[i]);
//    }
//    return 0;
//}
//#include <stdio.h>
//int main()
//{
//    int a = 0x11223344;
//    char* pc = (char*)&a;
//    *pc = 0;
//    printf("%x\n", a);
//    return 0;
//}

//写一个函数，可以逆序一个字符串的内容。
#include <stdio.h>
#include <string.h>
void reverse_string(char* ptr)
{
	int left = 0;
	int right = strlen(ptr) - 1;
	while (left < right)
	{
		
		char tmp = ptr[left];
		ptr[left] = ptr[right];
		ptr[right] = tmp;

		left++;
		right--;

	}
	printf("%s\n", ptr);
}
int main()
{
	char arr[20] = "abcdef";
	
	reverse_string(arr);
	return 0;
}