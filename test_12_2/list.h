#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
typedef int Datatype;
typedef struct ListNode
{
	Datatype data;
	struct ListNode* next;
}Node;


void print(Node* phead);//打印链表
void pushfront(Node** pphead, Datatype x);//链表头插
void pushback(Node** pphead, Datatype x);//链表尾插

Node* find(Node* pphead, Datatype x);//查找链表
void SLinsert(Node** pphead, Node* pos, Datatype x);//在pos之前插入

void SLinstertAfter(Node* pos, Datatype x);//在pos之后插入

//删除pos位置的值
void STerase(Node** pphead, Node* pos);

//删除pos位置后面的值
void STeraseafter(Node* pos);

void SLTDestroy(Node** pphead);//销毁链表

Node** reserveList(Node** list);//反转链表
