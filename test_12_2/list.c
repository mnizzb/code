#include"list.h"
Node* buyNode(Datatype x)
{
	Node* newnode = (Node*)malloc(sizeof(Node));
	if (newnode == NULL)
	{
		perror("malloc fail");
		return NULL;
	}
	newnode->data = x;
	newnode->next = NULL;
	return newnode;
}
void pushfront(Node** phead, Datatype x)
{
	assert(phead);
	Node* newnode = buyNode(x);
	newnode->next = *phead;
	*phead = newnode;
}
void pushback(Node** phead, Datatype x)
{
	assert(phead);
	Node* newnode = buyNode(x);
	Node* tail = *phead;
	while (tail->next != NULL)
	{
		tail = tail->next;

	}
	tail->next = newnode;
	newnode->next = NULL;

}

void print(Node* phead)
{
	Node* cur = phead;
	while (cur != NULL)
	{
		printf("%d->", cur->data);
		cur = cur->next;
	}
	printf("NULL\n");
}
Node* find(Node* pphead, Datatype x)
{
	assert(pphead);
	Node* tail = pphead;
	while (tail)
	{
		if (tail->data == x)
		{
			return tail;
		}
		tail = tail->next;
	}
	return NULL;
}

void SLinsert(Node** pphead, Node* pos, Datatype x)
{
	assert(*pphead);
	Node* cur = *pphead;
	Node* newnode = buyNode(x);
	while (cur->next != pos)
	{
		cur = cur->next;
	}
	cur->next = newnode;
	newnode->next = pos;
}
void SLinstertAfter(Node* pos, Datatype x)
{
	assert(pos);
	Node* newnode = buyNode(x);
	Node* tail = pos->next;
	pos->next = newnode;
	newnode->next = tail;
}
void STerase(Node** pphead, Node* pos)
{
	assert(*pphead);
	Node* last = *pphead;
	Node* next = pos->next;
	while (last->next != pos)
	{
		last = last->next;
	}
	last->next = next;
	free(pos);
	pos = NULL;

}
void STeraseafter(Node* pos)
{
	assert(pos);
	Node* tail = pos->next;
	pos->next = tail->next;
	free(tail);
	tail = NULL;
}
void SLTDestroy(Node** pphead)
{
	assert(*pphead);
	Node* tail = *pphead;
	while (tail)
	{
		Node* next = tail->next;
		free(tail);
		tail = next;
	}
	*pphead = NULL;
}
Node** reserveList(Node** list)
{
	Node* oldhead = *list;
	Node* newhead = NULL;
	while (oldhead != NULL)
	{
		Node* tmp = oldhead->next;
		oldhead->next = newhead;
		newhead = oldhead;
		oldhead = tmp;
	}
	return  newhead;
}