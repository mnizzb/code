#define _CRT_SECURE_NO_WARNINGS
//#include <stdio.h>
//#include <assert.h>
////字符串左旋
////实现一个函数，可以左旋字符串中的k个字符。
//void reverse(char* left, char* right)
//{
//	assert(left && right);
//	while (left < right)
//	{
//	
//		char tmp = *left;
//		*left = *right;
//		*right = tmp;
//		left++;
//		right--;
//	}
//}
//void left_move(char* arr, int k)
//{
//	int ret = strlen(arr);
//	//左边置换
//	reverse(arr, arr + k - 1);
//	//右边置换
//	reverse(arr + k, arr + ret - 1);
//	//总体置换
//	reverse(arr, arr + ret - 1);
//}
//int main()
//{
//	char arr[] = "abcdef";
//	int k = 0;
//	scanf("%d", &k);
//	left_move(arr, k);
//	printf("%s\n", arr);
//	return 0;
//}


//写一个函数，判断一个字符串是否为另外一个字符串旋转之后的字符串。
//例如：给定s1 = AABCD和s2 = BCDAA，返回1
//给定s1 = abcd和s2 = ACBD，返回0.
//AABCD左旋一个字符得到ABCDA
//AABCD左旋两个字符得到BCDAA
//AABCD右旋一个字符得到DAABC
#include <stdio.h>
#include <string.h>
int left_move(char* arr1, char* arr2)
{
	int ret = strlen(arr1);
	int ret1 = strlen(arr2);
	if (ret != ret1)//判断这两个字符串的长度是否相等
		return 0;
	int i = 0;
	for (i = 0; i < ret; i++)
	{
		int j = 0;
		char tmp1 = *arr1;
		for (j = 0; j < ret - 1; j++)
		{
			*(arr1 + j) = *(arr1 + j + 1);
		}
		*(arr1 + ret - 1) = tmp1;
		if (strcmp(arr1, arr2) == 0)
			return 1;
	}
	return 0;
}
int main()
{

	char arr1[] = "ABCDEF";
	char arr2[] = "CDEFAB";
	int ret = left_move(arr1, arr2);
	printf("%d\n", ret); 
	return 0;
}