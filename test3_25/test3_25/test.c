#define _CRT_SECURE_NO_WARNINGS
//求1!+2!+3!+4!，，，+10!的值
#include <stdio.h>
//int main()
//{
//	int i = 0;
//	int sum = 0;
//	int ret = 1;
//	int n = 0;
//	for (n = 1; n <= 10; n++)
//	{	
//		ret = 1;//ret要重置为1，确保ret的值不会保留上次循环之后的值。
//		for (i = 1; i <= n; i++)
//		{
//			ret = ret * i;
//		}
//		//sum = sum + ret;
//		sum += ret;
//	}
//	printf("%d\n", sum);
//	return 0;
//}
//优化之后：
//int main()
//{
//		int i = 0;
//		int sum = 0;
//		int ret = 1;
//		
//		for (i = 1; i <= 3; i++)
//			{
//				ret = ret * i;
//				sum += ret;
//			}
//			
//	
//		printf("%d\n", sum);
//		return 0;
//}
//二分查找
//必须是有序数组才能使用
int main()
{
	int i = 0;
	int arr[] = {1,2,3,4,5,6,7,8,9,10};
	int w = 7;
	int sz = sizeof(arr) / sizeof(arr[0]);
	int left = 0;
	int right = sz - 1;;
	int flag = 1;
	while (left <= right) {
		int mid = (left + right) / 2;
		if (arr[mid] == w)
		{
			printf("找到了,下标是：%d\n",mid);
			flag = 0;
			break;
		}
		else if (arr[mid] < w)
		{
			left = mid + 1;

		}
		else if (arr[mid] > w)
		{
			right = mid - 1;
		}
		
	
	}
	if (flag == 1)
	{
		printf("找不到\n");
	}
	return 0;

}