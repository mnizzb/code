#define _CRT_SECURE_NO_WARNINGS
//实现一个函数is_prime，判断一个数是不是素数。

//利用上面实现的is_prime函数，打印100到200之间的素数。
//#include <stdio.h>
//
//void is_prime()
//{
//	int i = 0;
//	int j = 0;
//	for (i = 100; i <= 200; i++)
//	{
//		int flag = 0;
//		for (j = 2; j < i; j++)
//		{
//			if (i % j == 0)
//			{
//				flag = 1;
//				break;
//			}
//		
//		}
//		if (flag == 0)
//		{
//			printf("%d ", i);
//		}
//	}
//	
//
//	
//}
//int main()
//{
//	is_prime();
//
//	return 0;
//}

//实现函数判断year是不是闰年。
//#include <stdio.h>
//
//int is_year(int i)
//{
//
//	return (i % 4 == 0 && i % 100 == 0 || i % 400 == 0);
//}
//int main()
//{	
//	int i = 0;
//	while (1)
//	{
//		scanf("%d", &i);
//		int j = is_year(i);
//		if (j == 0)
//			printf("不是闰年\n");
//		
//
//		else
//			printf("是闰年\n");
//	}
//
//
//	return 0;
//}
//实现一个函数来交换两个整数的内容。
//#include <stdio.h>
//void swap(int *a, int* b)
//{
//	int tmp = *a;
//	*a = *b;
//	*b = tmp;
//}
//int main()
//{
//	int a = 7;
//	int b = 8;
//	printf("交换之前的值：%d %d\n", a, b);
//	swap(&a,&b);//这里使用的是传址调用，传值调用实现不了
//	printf("交换之后的值：%d %d\n", a, b);
//	return 0;
//}

//实现一个函数，打印乘法口诀表，口诀表的行数和列数自己指定

//如：输入9，输出9 * 9口诀表，输出12，输出12 * 12的乘法口诀表。
#include <stdio.h>

void mult(int a)
{
	int i = 0;
	int j = 0;
	for (i = 1; i <= a; i++)
	{
		int j = 0;
		for (j = 1; j <= i; j++)
		{
			int sum = i * j;
			printf("%d * %d = %d ", j, i, sum);
		}
		printf("\n");
	}
}
int main()
{
	int a = 0;
	scanf("%d", &a);
	mult(a);
	return 0;
}