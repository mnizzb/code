#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
//有一个数字矩阵，矩阵的每行从左到右是递增的，矩阵从上到下是递增的，
// 
//要求：时间复杂度小于O(N);
int main()
{
	int arr[3][3] = { 1,2,3,4,5,6,7,8,9 };
	int k = 0;
	scanf("%d", &k);
	int i = 0;
	int j = 2;
	int flag = 0;
	while (i < 3 && j >= 0)
	{
		
		if (arr[i][j] < k)
		{
			i++;
		}
		else if (arr[i][j] > k)
		{
			j--;
		}
		else
		{
			printf("找到了：%d %d\n", i,j);
			flag = 1;
			break;
		}
		

	}
	if (flag == 0)
	{
		printf("找不到");
	}
	

	return 0;
}