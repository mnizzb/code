#define _CRT_SECURE_NO_WARNINGS
//#include <stdio.h>
//int main()
//{
//	int a, b, c;
//	a = 5;
//	c = ++a;
//	b = ++c, c++, ++a, a++;
//	b += a++ + c;
//	printf("a = %d b = %d c = %d\n:", a, b, c);
//	return 0;
//}

//00000000000000000000000000010110   22
//00000000000000000000000000100001   33
//00000000000000000000000000110111   55
//00000000000000000000000000000001  `1(与55想与)


//求两个数二进制中不同位的个数
//#include <stdio.h>
//int main()
//{
//	int a = 0;
//	int b = 0;
//	int count = 0;
//	scanf("%d %d", &a, &b);
//	int num=a^ b;
//	while (num)
//	{
//		if (num & 1 == 1)
//			count++;
//		num = num >> 1;
//	}
//	printf("%d", count);
//	return 0;
//}

//写一个函数返回参数二进制中 1 的个数。
//#include <stdio.h>
//int main()
//{
//
//	int a = 0;
//	int i = 0;
//	int count = 0;
//	scanf("%d", &a);
//	while (a !=0)
//	{
//		a &= a - 1;//这样既可以求整数也可以求负数
//		count++;
//		
//	}
//	printf("%d", count);
//	return 0;
//}


//获取一个整数二进制序列中所有的偶数位和奇数位，分别打印出二进制序列
#include <stdio.h>
int main()
{
	int i = 0;
	int a = 0;
	int num = 0;
	scanf("%d", &a);
	printf("偶数位：");
	for (i = 30; i >=0; i -=2)
	{
		
		num = (a >> i) & 1;
		printf("%d", num);
	}
	printf("\n");
	printf("奇数位：");
	for (i = 31; i >= 1; i -= 2)
	{
		num = (a >> i) & 1;
		printf("%d", num);
	}
	return 0;
}