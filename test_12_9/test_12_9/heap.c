﻿#include"heap.h"
void Swap(int* a, int* b)
{
	int tmp = *a;
	*a = *b;
	*b = tmp;
}
void AdjustUp(HPDataType* a, int child)
{
	assert(a);
	int parent = (child - 1) / 2;
	while (child > 0)
	{
		if (a[child] < a[parent])
		{
			Swap(&a[child], &a[parent]);
			child = parent;
			parent = (child - 1) / 2;

		}
		else
		{
			break;
		}
	}
}
void AdjustDown(HPDataType* a, int n, int parent)
{
	int child = 2 * parent + 1;
	while (child < n)
	{
		if (child + 1 < n && a[child + 1] < a[child])
		{
			++child;
		}
		if (a[parent] > a[child])
		{
			Swap(&a[parent], &a[child]);
			parent = child;
			child = 2 * parent + 1;
		}
		else
		{
			break;
		}
	}
}
void heapinit(heap* hp, HPDataType* a, int n)
{
	assert(hp);
	HPDataType* tmp = (HPDataType*)malloc(sizeof(HPDataType) * n);
	if (tmp == NULL)
	{
		perror("malloc fail");
		exit(-1);
	}
	hp->a = tmp;
	memcpy(hp->a, a, sizeof(HPDataType) * n);
	hp->size = 0;
	hp->capacity = n;
	int i = 0;
	//С
	for (i = (hp->size - 1 - 1) / 2; i >= 0; i--)
	{
		AdjustDown(hp->a, hp->size, i);
	}

}
void heapdestroy(heap* hp)
{
	assert(hp);
	free(hp->a);
	hp->a = NULL;
	hp->size = hp->capacity = 0;
}

void heappush(heap* hp, HPDataType x)
{
	assert(hp);
	if (hp->size == hp->capacity)
	{
		HPDataType* tmp = (HPDataType*)realloc(hp->a, sizeof(HPDataType) * 2 * hp->capacity);
		if (tmp == NULL)
		{
			perror("malloc fail");
			exit(-1);
		}
		hp->a = tmp;
		hp->capacity *= 2;
	}
	hp->a[hp->size] = x;
	hp->size++;
	AdjustUp(hp->a, hp->size - 1);
}

bool heapempty(heap* hp)
{
	assert(hp);
	return hp->size == 0;
}
void heappop(heap* hp)
{
	assert(hp);
	assert(!heapempty(hp));
	Swap(&hp->a[0], &hp->a[hp->size - 1]);
	hp->size--;
	AdjustDown(hp->a, hp->size, 0);
}

HPDataType heaptop(heap* hp)
{
	assert(hp);
	return hp->a[0];
}

HPDataType heapsize(heap* hp)
{
	assert(hp);
	return hp->size;
}
//
//int* getLeastNumbers(heap* hp, int arrSize, int k)
//{
//	
//	for (int i = (arrSize - 1 - 1) / 2; i >= 0; i--)
//	{
//		AdjustDown(hp->a, arrSize, i);
//	}
//	
//	int end = arrSize - 1;
//	while (end > 0)
//	{
//		Swap(&hp->a[0], &hp->a[end]);
//		AdjustDown(hp->a, end, 0);
//		end--;
//	}
//
//	int* arr = (int*)malloc(sizeof(int) * k);
//	if (arr == NULL)
//	{
//		perror("malloc fail");
//		exit(-1);
//	}
//	int i = 0;
//	for (i = 0; i < k; i++);
//	{
//		arr[i] = hp->a[i];
//	}
//	return arr;
//}

int* getLeastNumbers(heap* hp, int k)
{
	int i = 0;
	for (i = (hp->size - 1 - 1) / 2; i >= 0; i--)
	{
		AdjustDown(hp->a, hp->size, i);
	}
	int* arr = (int*)malloc(sizeof(int) * k);
	if (arr == NULL)
	{
		perror("malloc fail");
		exit(-1);
	}

	int end = hp->size - 1;
	for (int i = 0; i < k; i++)
	{
		arr[i] = hp->a[0];//先取出堆顶的数据
		Swap(&hp->a[0], &hp->a[end]);
		AdjustDown(hp->a, end, 0);
		end--;
	}
	return arr;
}