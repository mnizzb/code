#pragma once
#include<stdio.h>
#include<assert.h>
#include<stdlib.h>
#include<stdbool.h>
#include<string.h>
typedef int HPDataType;
typedef struct Heap
{
	HPDataType* a;
	int size;
	int capacity;
}heap;

void heapinit(heap* hp, HPDataType* a, int n);//初始化建堆

void heapdestroy(heap* hp);//销毁堆

void heappush(heap* hp, HPDataType x);//入堆

void heappop(heap* hp);//出堆

HPDataType heaptop(heap* hp);//堆顶元素

HPDataType heapsize(heap* hp);//堆内的个数

int* getLeastNumbers(heap* hp, int k);