#include"stack.h"

void StackInit(Stack* st)
{
	st->a = (STDataType*)malloc(sizeof(STDataType) * 4);
	if (st->a == NULL)
	{
		perror("malloc fail");
	}
	st->top = 0;
	st->capacity = 4;
}
void StackDestroy(Stack* st)
{
	assert(st);
	free(st->a);
	st->a = NULL;
	st->capacity = st->top = 0;
}

void StackPush(Stack* st, STDataType x)
{
	if (st->top == st->capacity)
	{
		STDataType* tmp = (STDataType*)realloc(st->a, sizeof(STDataType)*2 * st->capacity);
		if (tmp == NULL)
		{
			perror("realloc fail");
			exit(-1);
		}
		st->a = tmp;
		st->capacity *= 2;
	}
	st->a[st->top] = x;
	st->top++;
}