#include<iostream>
using namespace std;
//typedef int Datatype;
//class Stack
//{
//public:
//	Stack(size_t capacity = 4)
//	{
//		_a = (Datatype*)malloc(sizeof(Datatype)* capacity);
//		if (_a == NULL)
//		{
//			perror("malloc fail");
//			return;
//		}
//		_size = 0;
//		_capacity = capacity;
//	}
//	void Push(Datatype x)
//	{
//		_a[_size] = x;
//		_size++;
//	}
//	
//	~Stack()
//	{
//		if (_a)
//		{
//			cout << "~Satck()" << endl;
//			free(_a);
//			_a = NULL;
//			_size = _capacity = 0;
//		}
//		
//	}
//private:
//	Datatype* _a;
//	int _size;
//	int _capacity;
//};
//int main()
//{
//	Stack s1 = 10;
//	s1.Push(1);
//	s1.Push(2);
//	s1.Push(3);
//	s1.Push(4);
//
//	return 0;
//}
//
//class Time
//{
//public:
//	~Time()
//	{
//		cout << "~Time()" << endl;
//	}
//private:
//	int _hour;
//	int _minute;
//	int _second;
//};
//
//class Date
//{
//private:
//	//内置类型
//	int _year = 1970;
//	int _month = 1;
//	int _day = 1;
//	//自定义类型
//	Time t;
//};
//int main()
//{
//	Date d;
//	return 0;
//}
//
////程序运行结束后输出：~Time()
////在main方法中根本没有直接创建Time类的对象，为什么最后调用Time类的析构函数？
////因为：main方法中创建了Date对象d，而d中包含4个成员变量，其中_year,_month,_day三个是内置成员类型成员，销毁时不需要
////
// 资源清理，最后系统直接将其内存回收即可；而_t是Time类对象，所以在d销毁时，要将其内部包含的Time类的_t对象销毁，所以要调用Time类的析构函数。

//class Date
//{
//public:
//	Date(int year = 1900, int month = 1, int day = 1)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//	Date(const Date& d)
//	{
//		_year = d._year;
//		_month = d._month;
//		_day = d._day;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//int main()
//{
//	Date d1;
//	Date d2(d1);
//	return 0;
//}

//class Time
//{
//public:
//	Time()
//	{
//		_hour = 1;
//		_minute = 1;
//		_second = 1;
//	}
//	Time(const Time& t)
//	{
//		_hour = t._hour;
//		_minute = t._minute;
//		_second = t._second;
//		cout << "Time(const Time& t)" << endl;
//	}
//private:
//	int _hour;
//	int _minute;
//	int _second;
//};
//
//class Date
//{
//private:
//	int _year = 1970;
//	int _month = 1;
//	int _day = 1;
//
//	Time _t;
//};
//
//int main()
//{
//	Date d1;
//
//	Date d2(d1);
//	return 0;
//}

//class Date
//{
//public:
//
//	Date(int year = 2024, int month = 10, int day = 15)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//	bool operator==(const Date& d1)
//	{
//		return _year == d1._year
//			&& _month == d1._month
//			&& _day == d1._day;
//	}
//	Date& operator=(const Date& d)
//	{
//		if (this!=&d)
//		{
//			_year = d._year;
//			_month = d._month;
//			_day = d._day;
//		}
//		return *this;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};

//typedef int DataType;
//class Stack
//{
//public:
//	Stack(size_t capacity = 10)
//	{
//		_array = (DataType*)malloc(capacity * sizeof(DataType));
//		if (nullptr == _array)
//		{
//			perror("malloc申请空间失败");
//			return;
//		}
//		_size = 0;
//		_capacity = capacity;
//	}
//	void Push(const DataType& data)
//	{
//		// CheckCapacity();
//		_array[_size] = data;
//		_size++;
//	}
//	~Stack()
//	{
//		if (_array)
//		{
//			free(_array);
//			_array = nullptr;
//			_capacity = 0;
//			_size = 0;
//		}
//	}
//private:
//	DataType* _array;
//	size_t _size;
//	size_t _capacity;
//};
//int main()
//{
//	Stack s1;
//	s1.Push(1);
//	s1.Push(2);
//	s1.Push(3);
//	s1.Push(4);
//	Stack s2;
//	s2 = s1;
//	return 0;
//}

//s1和s2共用同一个内存，当s2析构后s1再去访问这块内存就会造成访问违规，同一块内存不能被释放两次

//class Date
//{
//public:
//
//	Date(int year = 2024, int month = 10, int day = 15)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//
//	//前置++  返回+1之后的结果
//	//注意：this指向的对象函数结束后不会销毁，故以引用方式返回提高效率
//	Date& operator++()
//	{
//		_day += 1;
//		return *this;
//	}
//	//后置++	注意：后置++是先使用后+1，因此需要返回+1之前的旧值，故需在实现时需要先将this保存
//	//一份，然后给this + 1
//	//而temp是临时对象，因此只能以值的方式返回，不能返回引用
//	Date operator++(int)
//	{
//		Date tmp(*this);
//		_day += 1;
//		return tmp;
//	}
//	void Print()
//	{
//		cout << _year << "/" << _month << "/" << _day << endl;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//
//int main()
//{
//	Date d;
//	Date d1(2024, 10, 15);
//	d = d1++;//先使用再++
//	d.Print();
//	d = ++d1;//先++再使用
//	d.Print();
//	return 0;
//}

//class Date
//{
//public:
//	Date(int year, int month, int day)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//	void Print()
//	{
//		cout << "Print()" << endl;
//		cout << "year:" << _year << endl;
//		cout << "month:" << _month << endl;
//		cout << "day:" << _day << endl << endl;
//	}
//	void Print() const
//	{
//		cout << "Print()const" << endl;
//		cout << "year:" << _year << endl;
//		cout << "month:" << _month << endl;
//		cout << "day:" << _day << endl << endl;
//	}
//private:
//	int _year; // 年
//	int _month; // 月
//	int _day; // 日
//};
//void Test()
//{
//	Date d1(2022, 1, 13);
//	d1.Print();
//	const Date d2(2022, 1, 13);
//	d2.Print();
//}
//int main()
//{
//	Test();
//	return 0;
//}

class Date
{
public:
	Date* operator&()
	{
		return this;
	}
	const Date* operator&()const
	{
		return this;
	}
private:
	int _year; // 年
	int _month; // 月
	int _day; // 日
};