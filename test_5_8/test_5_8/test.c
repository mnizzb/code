#define _CRT_SECURE_NO_WARNINGS  1
#include <stdio.h>
//使用回调函数模拟实现qsort（冒泡排序）
void _swap(void* base,void*base1,int size)
{
	int i = 0;
	for (i = 0; i < size; i++)
	{
		char tmp = 0;
		tmp = *((char*)base + i);
		*((char*)base + i) = *((char*)base1 + i);
		*((char*)base1 + i) = tmp;
	}
}
int int_cmp(void *p1,void *p2)
{
	return *(int*)p1 - *(int*)p2;
}
void bulle_qsort(void* base, int count, int size, int (*int_cmp)(void*, void*))
{
	int i = 0;
	int j = 0;
	for (i = 0; i < count; i++)
	{
		for (j = 0; j < count - 1 - i; j++)
		{
			if (int_cmp((char*)base + j * size, (char*)base + (j + 1) * size) > 0)
			{
				_swap((char*)base + j * size, (char*)base + (j + 1) * size, size);
			}
		}
	}
}
int main()
{
	int arr[] = { 1,3,6,2,9,2,4,5,3,0 };
	bulle_qsort(arr, sizeof(arr) / sizeof(arr[0]), sizeof(int), int_cmp);
	int i = 0;
	for (i = 0; i < sizeof(arr) / sizeof(arr[0]); i++)
	{
		printf("%d ", arr[i]);
	}
	

	return 0;
}