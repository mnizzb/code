#define _CRT_SECURE_NO_WARNINGS
//使用递归的方式打印每一个数
//#include <stdio.h>
//void print(int i)
//{
//	if (i > 10)
//	{
//		print(i / 10);
//	}
//	printf("%d ", i % 10);
//}
//int main()
//{
//	int i = 0;
//	scanf("%d", &i);
//	print(i);
//	return 0;
//}
// 
//递归和非递归分别实现求n的阶乘（不考虑溢出的问题）
//非递归的方式如下：
//#include <stdio.h>
//int main()
//{
//	int i = 0;
//	int n = 0;
//	int let = 1;
//	scanf("%d", &n);
//	for (i = 1; i <= n; i++)
//	{
//		let = let * i;
//	}
//	printf("%d", let);
//	return 0;
//}

//递归的方式如下：
//#include <stdio.h>
//int print(int n)
//{
//	if (n > 1)
//	{
//		return n * print(n - 1);
//	}
//	else
//		return 1;
//}
//int main()
//{	
//	int num = 0;
//	scanf("%d", &num);
//	int i = print(num);
//	printf("%d",i);
//	return 0;
//}

//递归和非递归分别实现strlen
//计算字符串的长度是多少
//非递归：
//#include <stdio.h>
//void is_stlen(char* n)
//{
//	int count = 0;
//	while (1) 
//	{
//		if (*n != '\0')
//		{
//			count++;
//			n = n + 1;
//		}
//		else
//			break;
//	}
//
//	printf("%d", count);
//}
//int main()
//{
//	char arr[] = "abcd";
//	is_stlen(arr);
//	return 0;
//}

//递归：
//#include <stdio.h>
//int is_strlen(char* arr)
//{
//	if (*arr == '\0')
//		return 0;
//	else
//		return 1 + is_strlen(arr + 1);
//}
//int main()
//{
//	int num = 0;
//	char arr[] = "abcd";//字符串采用char类型来定义
//	int len = is_strlen(arr);//arr是数组的第一个元素
//	printf("%d", len);
//	return 0;
//}
//
//编写一个函数 reverse_string(char * string)（递归实现）

//实现：将参数字符串中的字符反向排列，不是逆序打印。

//要求：不能使用C函数库中的字符串操作函数。